package io.flutter.plugins;

import io.flutter.plugin.common.PluginRegistry;
import de.pandaswelt.backendwrapper.BackendWrapperPlugin;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
  public static void registerWith(PluginRegistry registry) {
    if (alreadyRegisteredWith(registry)) {
      return;
    }
    BackendWrapperPlugin.registerWith(registry.registrarFor("de.pandaswelt.backendwrapper.BackendWrapperPlugin"));
  }

  private static boolean alreadyRegisteredWith(PluginRegistry registry) {
    final String key = GeneratedPluginRegistrant.class.getCanonicalName();
    if (registry.hasPlugin(key)) {
      return true;
    }
    registry.registrarFor(key);
    return false;
  }
}
