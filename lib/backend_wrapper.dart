import 'dart:async';

import 'package:flutter/services.dart';
import 'api/user_information.dart';
import 'models/user.dart';

class BackendWrapper {
  String _apiKey;

  BackendWrapper(this._apiKey);

  static const MethodChannel _channel =
      const MethodChannel('backend_wrapper');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  Future<User> retrieveUserInformation() {
    return UserInformation(_apiKey).fetchPost().then((value) => value as User);
  }




}
