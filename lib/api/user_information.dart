import 'package:http/http.dart';

import 'base_call.dart';
import '../models/user.dart';

class UserInformation {
  String userApiKey;

  UserInformation(
      this.userApiKey
  );


  Future<dynamic> fetchPost() {
    return BaseCall.handleData(
        get(
            BaseCall.buildUrl(
              type: "user",
              userApiKey: userApiKey,
              ressource: "user-information",
              parameter: null
            )
        ),
        User.fromJson
    );
  }
}