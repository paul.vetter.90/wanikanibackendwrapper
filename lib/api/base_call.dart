import 'package:http/http.dart';
import 'dart:async';
import 'dart:convert';
import 'package:meta/meta.dart';

class BaseCall{
  static dynamic extractUserInformation(Map<String, dynamic> json, factory(Map<String, dynamic> json)){
    return factory(json["user_information"]);
  }

  static dynamic extractRequestedInformation(Map<String, dynamic> json, factory(Map<String, dynamic> json)){
    return factory(json["requested_information"]);
  }

  static Uri buildUrl(
      {@required String type, @required userApiKey: String, @required String ressource, String parameter}
  ){
    return Uri.parse("https://www.wanikani.com/api/$type/$userApiKey/$ressource/${parameter != null ? parameter : ""}");
  }

  static Future<dynamic> handleData(Future<Response> futureResponse, factory(Map<String, dynamic> json)) async{
    final response = await futureResponse;
    switch(response.statusCode){
      case 200: {
        var jsonObj = json.decode(response.body);
        var requestedInformation = extractRequestedInformation(jsonObj, factory);
        return requestedInformation != null ? requestedInformation : extractUserInformation(jsonObj, factory);
      }
      case 403: {
        throw Exception("Limit exceeded");
      }
      case 404: {
        throw Exception("Not Found");
      }
      default: {
        throw Exception("${response.statusCode}");
      }
    }
  }
}