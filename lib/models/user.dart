class User {
  final String username;
  final String gravatar;
  final int level;
  final String title;
  final String about;
  final String website;
  final String twitter;
  final int topics_count;
  final int posts_count;
  final DateTime creation_date;
  final DateTime vacation_date;

  User(
      this.username,
      this.gravatar,
      this.level,
      this.title,
      this.about,
      this.website,
      this.twitter,
      this.topics_count,
      this.posts_count,
      this.creation_date,
      this.vacation_date
      );

  static User fromJson(Map<String, dynamic> json){
    try {
      return new User(
          json['username'],
          json['gravatar'],
          json['level'],
          json['title'],
          json['about'],
          json['website'],
          json['twitter'],
          json['topics_count'],
          json['posts_count'],
          json['creation_date'] != null ? DateTime.fromMillisecondsSinceEpoch(json['creation_date']): null,
          json['vacation_date'] != null ? DateTime.fromMillisecondsSinceEpoch(json['vacation_date']) : null
      );
    } catch (e){
      return null;
    }
  }

  Map<String, dynamic> toJson() =>
      {
        'username': username,
        'gravatar': gravatar,
        'level': level,
        'title': title,
        'about': about,
        'website': website,
        'twitter': twitter,
        'topics_count': topics_count,
        'posts_count': posts_count,
        'creation_date': creation_date != null ? creation_date.millisecondsSinceEpoch : null,
        'vacation_date': vacation_date != null ? vacation_date.millisecondsSinceEpoch : null,
      };

  @override
  String toString() {
    return toJson().toString();
  }

}