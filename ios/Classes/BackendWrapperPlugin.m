#import "BackendWrapperPlugin.h"
#import <backend_wrapper/backend_wrapper-Swift.h>

@implementation BackendWrapperPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBackendWrapperPlugin registerWithRegistrar:registrar];
}
@end
